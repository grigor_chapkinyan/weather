//
//  WeatherShowingViewModelTests.swift
//  WeatherTests
//
//  Created by Grigor Chapkinyan on 5/8/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import XCTest
@testable import Weather
import RxSwift
import RxCocoa
import RxTest
import RxBlocking

class WeatherShowingViewModelTests: XCTestCase {
    var weatherShowingViewModel: WeatherShowingViewModel!
    var countryDataManager: CountryDataManager!
    var disposeBag: DisposeBag!
    
    var cityWeathers: BehaviorRelay<[CityWeather]?>!
    var isLoading: BehaviorRelay<Bool>!
    var error: PublishRelay<(state: WeatherDataFetchingErrorState,type: RequestError)?>!
    var existDataForFetching: BehaviorRelay<Bool>!
    
    override func setUp() {
        disposeBag = DisposeBag()

        cityWeathers = BehaviorRelay(value: [])
        isLoading = BehaviorRelay(value: false)
        error = PublishRelay()
        existDataForFetching = BehaviorRelay(value: false)
        
        countryDataManager = CountryDataManager.shared
        
        let exp_1 = expectation(description: "Country Data Manager fetch")
        var countryToShow: CountryToShow!
        
        countryDataManager.createShownInstanceWithFullName(fullName: "Armenia") { (country) in
            guard let country = country else {return}
            
            countryToShow = country
            
            exp_1.fulfill()
        }
        
        wait(for: [exp_1], timeout: 1)
        
        weatherShowingViewModel = WeatherShowingViewModel(country: countryToShow)
        
        weatherShowingViewModel
            .cityWeathers
            .skip(1)
            .bind(to: cityWeathers)
            .disposed(by: disposeBag)
        
        weatherShowingViewModel
            .error
            .bind(to: error)
            .disposed(by: disposeBag)
        
        weatherShowingViewModel
            .isLoading
            .skip(1)
            .bind(to: isLoading)
            .disposed(by: disposeBag)
        
        weatherShowingViewModel
            .existDataForFetching
            .skip(1)
            .bind(to: existDataForFetching)
            .disposed(by: disposeBag)
        
        error
            .asObservable()
            .subscribe(onNext: { (error) in
                XCTAssertNil(error, "Error recieved: \(error!)")
            })
            .disposed(by: disposeBag)
    }
    
    override func tearDown() {
        weatherShowingViewModel = nil
        countryDataManager = nil
        disposeBag = nil
        
        cityWeathers = nil
        error = nil
        isLoading = nil
        existDataForFetching = nil
    }
    
    func testStartFetchingWeatherData() throws {
        weatherShowingViewModel.startFetchingWeatherData()
        
        let isLoadingInitialResult = try isLoading.toBlocking().first()!
        let existDataForFetchingInitialResult = try existDataForFetching.toBlocking().first()!
        let cityWeathersResult = try cityWeathers.skip(1).toBlocking().first()!
        
        let isLoadingLastResult = try isLoading.toBlocking().first()!
        let existDataForFetchingLastResult = try existDataForFetching.toBlocking().first()!
        
        XCTAssertEqual(isLoadingInitialResult, true,"IsLoading FAIL")
        XCTAssertEqual(existDataForFetchingInitialResult, true,"ExistDataForFetching FAIL")
        XCTAssertGreaterThan(cityWeathersResult!.count, 0,"City Weather Count is Zero")
        
        XCTAssertEqual(isLoadingLastResult, false,"IsLoading FAIL")
        XCTAssertEqual(existDataForFetchingLastResult, true,"ExistDataForFetching FAIL")
    }
    
    func testContinueFetchingWeatherData() throws {
        weatherShowingViewModel.startFetchingWeatherData()
        
        let _ = try cityWeathers.skip(1).toBlocking().first()!
        let _ = try isLoading.toBlocking().first()!
        let _ = try existDataForFetching.toBlocking().first()!
        
        weatherShowingViewModel.continueFetchingWeatherData()
        
        let isloadingInitialResult = try isLoading.toBlocking().first()!
        let existDataForFetchingInitialResult = try existDataForFetching.toBlocking().first()!
        let cityWeathersResult = try cityWeathers.skip(1).toBlocking().first()!
        
        let isloadingFinalResult = try isLoading.toBlocking().first()
        let existDataForFetchingFinalResult = try existDataForFetching.toBlocking().first()!
        
        XCTAssertEqual(isloadingInitialResult, true,"IsLoading FAIL")
        XCTAssertEqual(existDataForFetchingInitialResult, true,"ExistDataForFetching FAIL")
        XCTAssertGreaterThan(cityWeathersResult!.count, 0,"City Weather Count is Zero")
        
        XCTAssertEqual(isloadingFinalResult, false,"IsLoading FAIL")
        XCTAssertEqual(existDataForFetchingFinalResult, true,"ExistDataForFetching FAIL")
    }
    
    func testEndLoadingEndFetchFromDb() throws {
        // First fetching
        weatherShowingViewModel.startFetchingWeatherData()
        
        let isLoadingInitialResult = try isLoading.toBlocking().first()!
        let existDataForFetchingInitialResult = try existDataForFetching.toBlocking().first()!
        let cityWeathersResult = try cityWeathers.skip(1).toBlocking().first()!
        
        let isLoadingLastResult = try isLoading.toBlocking().first()!
        let existDataForFetchingLastResult = try existDataForFetching.toBlocking().first()!
        
        XCTAssertEqual(isLoadingInitialResult, true,"IsLoading FAIL")
        XCTAssertEqual(existDataForFetchingInitialResult, true,"ExistDataForFetching FAIL")
        XCTAssertGreaterThan(cityWeathersResult!.count, 0,"City Weather Count is Zero")
        
        XCTAssertEqual(isLoadingLastResult, false,"IsLoading FAIL")
        XCTAssertEqual(existDataForFetchingLastResult, true,"ExistDataForFetching FAIL")
        
        // End loading
        
        weatherShowingViewModel.endLoadingAndFetchFromDb()
        
        let cityWeathersFromDb = try cityWeathers.toBlocking().first()!
        let existDataForFetchingFinalResult = try existDataForFetching.skip(1).toBlocking().first()!
        let isLoadingFinalResult = try isLoading.toBlocking().first()!
        
        XCTAssertEqual(cityWeathersResult?.count, cityWeathersFromDb?.count)
        XCTAssertEqual(existDataForFetchingFinalResult, false)
        XCTAssertEqual(isLoadingFinalResult, false)
    }
}
