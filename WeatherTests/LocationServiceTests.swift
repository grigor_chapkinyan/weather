//
//  LocationServiceTests.swift
//  WeatherTests
//
//  Created by Grigor Chapkinyan on 5/8/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import XCTest
@testable import Weather

class LocationServiceTests: XCTestCase {

    var locationService: LocationService!
    
    override func setUp() {
        locationService = LocationService()
    }

    override func tearDown() {
        locationService = nil
    }
    
    func testGetCurrentCountryName() {
        // Enable Location
        
        let exp = expectation(description: "Location service get current country name")
        
        locationService.getCurrentCountryNames { tuple in
            guard let tuple = tuple else {return}
            
            print(tuple.0,tuple.1)
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 2)
    }
}
