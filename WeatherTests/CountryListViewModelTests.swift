//
//  CountryListViewModelTests.swift
//  WeatherTests
//
//  Created by Grigor Chapkinyan on 5/9/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import XCTest
@testable import Weather
import RxSwift
import RxCocoa
import RxBlocking

class CountryListViewModelTests: XCTestCase {
    var countryListViewModel: CountryListViewModel!
    var countries: BehaviorRelay<[Country]?>!
    var isLoading: BehaviorRelay<Bool>!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        countryListViewModel = CountryListViewModel()
        disposeBag = DisposeBag()
        
        countries = BehaviorRelay(value: nil)
        isLoading = BehaviorRelay(value: false)
        
        countryListViewModel
            .countries
            .observeOn(MainScheduler.instance)
            .skip(1)
            .bind(to: countries)
            .disposed(by: disposeBag)
            
        countryListViewModel
            .isLoading.observeOn(MainScheduler.instance)
            .skip(1)
            .bind(to: isLoading)
            .disposed(by: disposeBag)
    }
    
    override func tearDown() {
        countryListViewModel = nil
        countries = nil
        isLoading = nil
        disposeBag = nil
    }
    
    func testFetchingAllCountries() throws {
        countryListViewModel.fetchAllCountries()
        
        let isLoadingInitialResult = try isLoading.toBlocking().first()!
        let countriesResult = try countries.skip(1).toBlocking().first()!
        let isLoadingFinalResult = try isLoading.toBlocking().first()!
        
        XCTAssertEqual(isLoadingInitialResult, true)
        XCTAssertGreaterThan(countriesResult!.count, 0)
        XCTAssertEqual(isLoadingFinalResult, false)
    }
}
