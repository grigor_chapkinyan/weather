//
//  WeatherListViewModelTests.swift
//  WeatherListViewModelTests
//
//  Created by Grigor Chapkinyan on 5/7/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import XCTest
@testable import Weather
import RxBlocking
import RxSwift
import RxCocoa

class WeatherListViewModelTests: XCTestCase {
    
    var weatherListViewModel: WeatherListViewModel!
    var countryToUpdate: PublishRelay<Country>!
    var countriesToShow: BehaviorRelay<[CountryToShow]>!
    var loading: PublishRelay<Bool>!
    var countryDataManager: CountryDataManager!
    
    var disposeBag: DisposeBag!
    
    override func setUp() {
        countryToUpdate = PublishRelay()
        countriesToShow = BehaviorRelay(value: [])
        loading = PublishRelay()
        countryDataManager = CountryDataManager.shared
        
        disposeBag = DisposeBag()
        
        weatherListViewModel = WeatherListViewModel(countryToUpdate: countryToUpdate)
        
        weatherListViewModel.countriesToShow.bind(to: countriesToShow).disposed(by: disposeBag)
        weatherListViewModel.loading.bind(to: loading).disposed(by: disposeBag)
    }
    
    override func tearDown() {
        weatherListViewModel = nil
        countryToUpdate = nil
        countriesToShow = nil
        disposeBag = nil
        loading = nil
        countryDataManager = nil
    }
    
    func testCountryAdding()  throws {
        // Set Current Location to RU
        
        let exp = expectation(description: "Country data manager fetch all countries")
        
        weatherListViewModel.startFetchingCountries()
        
        let resultInitial = try countriesToShow.skip(1).toBlocking().first()
        
        XCTAssertNotNil(resultInitial)
        
        XCTAssertEqual(resultInitial!.last!.shortName, "RU","Not Russia")
        
        countryDataManager.getAllSortedByFullname { (countries) in
            let countryArmenia = countries.filter{ $0.shortName == "AM" }.first!
            
            self.countryToUpdate.accept(countryArmenia)
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 1.5)
        
        let result = try countriesToShow.toBlocking().first()!
        
        XCTAssertEqual(result.count, 2,"To much countries")
    }
    
    func testCurrentCountryFetching() throws {
        // Set Location TO RU
        
        countryDataManager.removeAllShownInstancesFromDb()
        
        weatherListViewModel.startFetchingCountries()
        
        let result = try countriesToShow.skip(1).toBlocking().first()
        
        XCTAssertNotNil(result)
        
        XCTAssertEqual(result!.first!.shortName, "RU","Not Russia")
        
        XCTAssertEqual(result!.count, 1,"To much countries")
    }
}
