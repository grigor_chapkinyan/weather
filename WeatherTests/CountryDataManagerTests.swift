//
//  CountryDataManagerTests.swift
//  WeatherTests
//
//  Created by Grigor Chapkinyan on 5/8/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import XCTest
@testable import Weather

class CountryDataManagerTests: XCTestCase {

    var countryDataManager: CountryDataManager!
    
    override func setUp() {
        countryDataManager = CountryDataManager.shared
    }
    
    override func tearDown() {
        countryDataManager = nil
    }
    
    func testShownInstancesCleaningFromDb() {
        let exp_1 = expectation(description: "Shown country creating")
        
        countryDataManager.createShownInstanceWithFullName(fullName: "Australia") { (countryToShow) in
            guard let _ = countryToShow else { return }
            
            exp_1.fulfill()
        }
        
        wait(for: [exp_1], timeout: 1)
        
        countryDataManager.removeAllShownInstancesFromDb()
        
        let exp_2 = expectation(description: "Clear DB")
        
        countryDataManager.getShownInstancesFromDb { (countriesToShow) in
            if countriesToShow.count == 0 {
                exp_2.fulfill()
            }
        }
        
        wait(for: [exp_2], timeout: 2)
    }
    
    func testCreateShownInstancesWithShortName() {
        let exp = expectation(description: "Country is created")
        
        countryDataManager.createShownInstanceWithShortName(shortName: "US") { (countryToShow) in
            guard let _ = countryToShow else {return}
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 1)
    }
    
    func testCreateShownInstancesWithFullName() {
        let exp = expectation(description: "Country is created")
        
        countryDataManager.createShownInstanceWithFullName(fullName: "Brazil") { (countryToShow) in
            guard let _ = countryToShow else {return}
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 1)
    }
    
    func testGetNotShownInstancesFromDb() {
        countryDataManager.removeAllShownInstancesFromDb()
        
        let exp_1 = expectation(description: "Shown instances were fetched")
        
        countryDataManager.getShownInstancesFromDb { (countries) in
            XCTAssertEqual(countries.count, 0,"Db was not cleaned")

            exp_1.fulfill()
        }
        
        wait(for: [exp_1], timeout: 1)
        
        var notShownInstancesInitialCount: Int!
        
        let exp_2 = expectation(description: "Not shown countries were fetched")
        
        countryDataManager.getNotShownInstances { (countries) in
            notShownInstancesInitialCount = countries.count
            
            exp_2.fulfill()
        }
        
        wait(for: [exp_2], timeout: 1)
        
        let exp_3 = expectation(description: "Shown Country was created")
        
        countryDataManager.createShownInstanceWithShortName(shortName: "AM") { (countryToShow) in
            guard let _ = countryToShow else {return}
            
            exp_3.fulfill()
        }
        
        wait(for: [exp_3], timeout: 1)
        
        let exp_4 = expectation(description: "Not shown countries were fetched")
        var notShownInstancesCurrentCount: Int!

        countryDataManager.getNotShownInstances { (countries) in
            notShownInstancesCurrentCount = countries.count
            
            exp_4.fulfill()
        }
        
        wait(for: [exp_4], timeout: 1)

        XCTAssertEqual(notShownInstancesCurrentCount, (notShownInstancesInitialCount - 1),"Country Data Manager is working wrong")
    }
}
