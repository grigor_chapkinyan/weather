//
//  Constants.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreGraphics

struct Constants {
    
    enum ImageNames: String {
        case weatherIcon = "weatherIcon"
        case countriesIcon = "countriesIcon"
        case turnLeft = "turnLeft"
        case turnRight = "turnRight"
        case placeholder = "placeholder"
        case arrow = "arrow"
        case reload = "reload"
    }
    
    enum Key: String {
        case coord = "coord"
        case sys = "sys"
        case weather = "weather"
        case main = "main"
        case id = "id"
        case name = "name"
        case cnt = "cnt"
        case list = "list"
        case nameUpper = "Name"
        case codeUpper = "Code"
        case lon = "lon"
        case lat = "lat"
        case temp = "temp"
        case country = "country"
        case icon = "icon"
        case fullName = "fullName"
    }
    
    enum LabelNames: String {
        case weather = "Weather"
        case countries = "Countries"
        case ok = "OK"
    }
    
    enum AlertMessage: String {
        case checkInternet = "Please Check Internet Connection."
    }
    
    enum AlertTitle: String {
        case noInternet = "No Internet Connection."
    }
    
    enum WeatherApi: String {
        case keyUrlPathToAppend = "&appid=ebce6c5b749d9a36bdac148ffebf481c"
        case baseUrlPathForSeveralCityFetchById = "http://api.openweathermap.org/data/2.5/group?id="
        case unitsUrlPathToAppend = "&units=metric"
        case imageBaseUrl = "http://openweathermap.org/img/wn/"
        case imageUrlLastPath = "@2x.png"
    }
    
    enum GeoCodeApi: String {
        case UrlPathStartPoint = "https://geocode.xyz/"
        case UrlPathEndPoint = "?json=1"
    }
    
    enum ResourceName: String {
        case countries = "Countries"
        case cities = "Cities"
    }
    
    enum FileFormat: String {
        case json = "json"
    }
    
    enum Symbol: String {
        case degree = "°"
    }
    
    enum CoreDataEntity: String {
        case weather = "Weather"
        case cityWeather = "CityWeather"
        case weatherCoord = "WeatherCoord"
        case weatherMain = "WeatherMain"
        case weatherSys = "WeatherSys"
        case weatherDescription = "WeatherDescription"
        case country = "Country"
        case countryToShow = "CountryToShow"
    }
    
    enum ErrorMessage: String {
        case decodeError = "Decode Error"
        case fetchError = "Fetch Error"
    }
    
    static let mainUpper = "Main"
    static let pullToRefresh = "Pull To Refresh"
    
    static let loadingCellHeight = CGFloat(40)
    static let cityWeatherInfoCellHeight = CGFloat(60)
    static let countryInfoCellHeight = CGFloat(55)
}
