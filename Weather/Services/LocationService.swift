//
//  LocationService.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService: NSObject {
    
    private let manager = CLLocationManager()
    private let networkService = NetworkService()
    private var fetchCountryName: (() -> ())?
    private var currentLocation: CLLocation? {
        didSet {
            fetchCountryName?()
        }
    }
    
    override init() {}
    
    func getCurrentCountryNames(completion: @escaping (_ countryNames: (shortName: String,fullName: String)?) -> () ) {
        manager.delegate = self
        setupFetchCountryNameBlock(completion: completion)
        
        switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse :
                manager.startUpdatingLocation()
                if currentLocation != nil {
                    fetchCountryName?()
                }
            
            case .denied, .restricted:
                DispatchQueue.main.async {
                    completion(nil)
                }
            
            case .notDetermined:
                manager.requestWhenInUseAuthorization()
            
            @unknown default:
                DispatchQueue.main.async {
                    completion(nil)
                }
        }
    }
    
    // MARK: Helpers
    
    private func setupFetchCountryNameBlock(completion: @escaping (_ countryNames: (shortName: String,fullName: String)?) -> ()) {
        fetchCountryName = { [weak self] in
            defer {
                self?.manager.stopUpdatingLocation()
            }
            
            guard let currentLocation = self?.currentLocation else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            self?.fetchCountryNamesByCoordinate(coordinate: currentLocation.coordinate) { (countryName) in
                DispatchQueue.main.async {
                    completion(countryName)
                }
            }
        }
    }
    
    private func fetchCountryNamesByCoordinate(coordinate: CLLocationCoordinate2D, completion: @escaping (_ countryNames: (shortName: String,fullName: String)?) -> () ) {
        
        let geoCodeApiUrlPath = Constants.GeoCodeApi.UrlPathStartPoint.rawValue + String(coordinate.latitude) + "," + String(coordinate.longitude) + Constants.GeoCodeApi.UrlPathEndPoint.rawValue
        
        networkService.requestData(urlString: geoCodeApiUrlPath, method: .get) { (data, requestError) in
            guard   requestError == nil,
                    let data = data,
                    let dataDict = try? JSONSerialization.jsonObject(with: data) as? [String : Any],
                    let countryShortName = dataDict["prov"] as? String,
                    let countryFullName = dataDict["country"] as? String   else { completion(nil); return }
            
            completion((shortName: countryShortName,fullName: countryFullName))
        }
    }
}

// MARK: CLLocationManagerDelegate

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
            case .denied, .restricted:
                fetchCountryName?()
            case .authorizedAlways, .authorizedWhenInUse:
                manager.startUpdatingLocation()
            default:
                break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        if currentLocation == nil {
            currentLocation = location
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if currentLocation == nil {
            fetchCountryName?()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        if error != nil {
            if currentLocation == nil {
                fetchCountryName?()
            }
        }
    }
}
