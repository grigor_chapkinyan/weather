//
//  NetworkService.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/1/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

enum RequestError {
    case weatherApiServerError
    case other
}

class NetworkService {
    
    func requestData( urlString: String, method: HTTPMethod,queryParametrs: [String:String]? = nil , completion: @escaping (_ resultJsonData: Data?,_ error: RequestError?)->()) {
        guard   let urlComponents = createUrlComponentsWith(queryParametrs: queryParametrs, urlString: urlString),
            let urlForRequest = urlComponents.url else {
                DispatchQueue.main.async {
                    completion(nil,.other)
                }
                return
        }
        
        var urlRequest = URLRequest(url: urlForRequest)
        urlRequest.httpMethod = method.rawValue
        
        URLSession.shared.dataTask(with: urlRequest) {[weak self] (data, response, error) in
            guard let self = self else { completion(nil,.other); return }
            
            if self.statusCodeIsWeatherApiErrorAboutToMuchRequests(response: response) {
                DispatchQueue.main.async {
                    completion(nil,.weatherApiServerError)
                }
            }
            
            guard error == nil else {
                DispatchQueue.main.async {
                    completion(nil,.other)
                }
                return
            }
            
            if self.statusCodeIsSuccessful(response: response) {
                if let data = data {
                    DispatchQueue.main.async {
                        completion(data,nil)
                    }
                    return
                }
            }
            
            DispatchQueue.main.async {
                completion(nil,.other)
            }
            
        }.resume()
        
    }
    
    // MARK: Helpers
    
    private func createUrlComponentsWith(queryParametrs: [String:String]? = nil, urlString: String) -> URLComponents? {
        guard var urlComponents = URLComponents(string: urlString) else { return nil }
        
        if let queryParametrs = queryParametrs {
            var queryItems = [URLQueryItem]()
            for queryParametr in queryParametrs {
                queryItems.append(URLQueryItem(name: queryParametr.key, value: queryParametr.value))
            }
            urlComponents.queryItems = queryItems
        }
        
        return urlComponents
    }
    
    private func statusCodeIsSuccessful(response: URLResponse?) -> Bool {
        guard let httpResponse = response as? HTTPURLResponse else { return false }
        
        return 200 ... 299 ~= httpResponse.statusCode
    }
    
    private func statusCodeIsWeatherApiErrorAboutToMuchRequests(response: URLResponse?) -> Bool {
        guard let httpResponse = response as? HTTPURLResponse else { return false }
        
        return httpResponse.statusCode == 429
    }
}
