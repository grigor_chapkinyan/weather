//
//  DBService.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/13/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

enum EntityProperty: String {
    case id = "id"
    case fullName = "fullName"
    case country = "country"
    case name = "name"
}

class DBService {
    
    static let sharedInstance = DBService()
    private init(){}
    
    lazy var managedContext = {
        return self.persistentContainer.viewContext
    }()
    
    // MARK: Core Data stack
    
    private(set) lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: Constants.CoreDataEntity.weather.rawValue)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                assertionFailure("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                assertionFailure("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: Get Object
    
    func getEntitiesAsync(name: String, predicate: NSPredicate?, sortDecriptors: [NSSortDescriptor]?, completion: @escaping (_ result: ([NSManagedObject])) -> ()) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: name)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDecriptors
        
        let asyncFetchRequest = NSAsynchronousFetchRequest<NSManagedObject>(fetchRequest: fetchRequest) { (result) in
            DispatchQueue.main.async {
                completion(result.finalResult ?? [])
            }
        }
        
        do {
            try managedContext.execute(asyncFetchRequest)
        }
        catch {
            print("Coudn't get entity, name: \(name), predicate: \(String(describing: predicate?.description))")
            DispatchQueue.main.async {
                completion([])
            }
        }
    }
    
    func getEntitiesSync(name: String, predicate: NSPredicate?, sortDecriptors: [NSSortDescriptor]?) -> [NSManagedObject] {
        var result = [NSManagedObject]()
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: name)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDecriptors
        
        do {
            result = try managedContext.fetch(fetchRequest)
        }
        catch {
            print("Coudn't get entity, name: \(name), predicate: \(String(describing: predicate?.description))")
        }
        
        return result
    }
    
    // MARK: Delete Existing Object
    
    func deleteEntityIfExist(entityName: String, propertyTypeForPredicate: EntityProperty?, propertyValue: String?) {
        defer {
            DBService.sharedInstance.saveContext()
        }
        
        var predicate: NSPredicate!
        
        if  let propertyTypeForPredicate = propertyTypeForPredicate,
            let propertyValue = propertyValue {
            predicate = NSPredicate(format: "\(propertyTypeForPredicate.rawValue) == %@", propertyValue as NSString)
        }

        let managedObjectsToDelete = getEntitiesSync(name: entityName, predicate: predicate, sortDecriptors: nil)
        
        for object in managedObjectsToDelete {
            managedContext.delete(object)
        }
    }
}

