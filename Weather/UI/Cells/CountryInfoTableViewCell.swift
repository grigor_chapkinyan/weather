//
//  CountryInfoTableViewCell.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/31/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit

class CountryInfoTableViewCell: UITableViewCell {
    
    static let reuseId = "CountryInfoTableViewCell"

    @IBOutlet private weak var nameLabel: UILabel!
    
    func setup(country: Country) {
        nameLabel.text = country.fullName
    }
}
