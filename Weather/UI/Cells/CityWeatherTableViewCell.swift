//
//  CityWeatherTableViewCell.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit
import SDWebImage

class CityWeatherTableViewCell: UITableViewCell {
    
    static let reuseId = "CityWeatherTableViewCell"
    
    @IBOutlet private weak var tempLabel: UILabel!
    @IBOutlet private weak var weatherImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!

    func setup(cityWeather: CityWeather) {
        tempLabel.text = String(Int(cityWeather.main.temp)) + Constants.Symbol.degree.rawValue
        nameLabel.text = cityWeather.name
        
        let weatherImageName = cityWeather.weather.icon
        setupWeatherImageView(imageName: weatherImageName)
    }

    // MARK: Helpers
    
    private func setupWeatherImageView(imageName: String?) {
        guard let imageName = imageName else {
            weatherImageView.image = UIImage(named: Constants.ImageNames.placeholder.rawValue)
            return
        }
        
        let pathToAppend = imageName + Constants.WeatherApi.imageUrlLastPath.rawValue
        let urlString =  Constants.WeatherApi.imageBaseUrl.rawValue + pathToAppend
        let imageUrl = URL(string: urlString)
        
        weatherImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: Constants.ImageNames.placeholder.rawValue))
    }
}
