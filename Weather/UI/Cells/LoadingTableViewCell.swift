//
//  LoadingTableViewCell.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/31/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    
    static let reuseId = "LoadingTableViewCell"
    
    private var loadingIndicatorView: UIActivityIndicatorView!
    
    func startAnimating() {
        loadingIndicatorView.startAnimating()
    }
    
    // Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLoadingIndicatorView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Helpers
    
    private func setupLoadingIndicatorView() {
        if #available(iOS 13.0, *) {
            self.loadingIndicatorView = UIActivityIndicatorView(style: .large)
            loadingIndicatorView.color = .systemOrange
        }
        else {
            self.loadingIndicatorView = UIActivityIndicatorView()
            loadingIndicatorView.color = .orange
        }
        
        UIViewController.addSubviewTo(view: loadingIndicatorView, superView: contentView, top: 0, bottom: 0, left: 0, right: 0)
    }
}
