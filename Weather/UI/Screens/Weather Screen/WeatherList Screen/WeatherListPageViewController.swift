//
//  WeatherListPageViewController.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol WeatherShowingScreenDelegate: AnyObject {
    func newCountryWasShown()
}

class WeatherListPageViewController: UIPageViewController {
    
    static let storyboardId = "WeatherListPageViewController"
    
    weak var delegateToNotify: WeatherShowingScreenDelegate?
    private var leftBarButtonItem: UIBarButtonItem!
    private var rightBarButtonItem: UIBarButtonItem!
    private var loadingIndicatorView: UIActivityIndicatorView?
    private var viewModel: WeatherListViewModel?
    private let countriesToShow: BehaviorRelay<[CountryToShow]> = BehaviorRelay(value: [])
    private let countryToUpdate = PublishRelay<Country>()
    private let isLoadingFirstCountry: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    private let disposeBag = DisposeBag()
    private var pages = [WeatherShowingViewController]()
    private var currentPageIndex: Int = 0
    private var pageTransitionIsInProgress = false
    private var firstFetchWasDone = false
    private var firstPageWasSet = false
    private var navigationBarTitle: String {
        guard pages.count > 0 else { return "" }
        
        return pages[currentPageIndex].countryName
    }
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMainView()
        setupLoadingIndicatorView()
        setupBarButtonItems()
        initViewModel()
        setupBindings()
        configureNavigationBar()
        startFetchingCountries()
    }
    
    // MARK: Helpers
    
    private func setupBindings() {
        viewModel?
            .countriesToShow
            .bind(to: countriesToShow)
            .disposed(by: disposeBag)
        
        viewModel?
            .loading
            .bind(to: isLoadingFirstCountry)
            .disposed(by: disposeBag)
            
        countriesToShow
            .asObservable()
            .skip(1)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (countries) in
                self?.notifyDelegateIfNeeded()
                self?.updateData()
                self?.configureNavigationBar()
            })
            .disposed(by: disposeBag)
        
        isLoadingFirstCountry
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (_) in
                self?.configureLoadingView()
            })
            .disposed(by: disposeBag)
    }
    
    private func initViewModel() {
        viewModel = WeatherListViewModel(countryToUpdate: countryToUpdate)
    }
    
    private func updateData() {
        updatePages()
        configureDelegation()
        showLastPage()
        configureDelegation()
    }
    
    private func configureNavigationBarTitle() {
        navigationController?.navigationBar.topItem?.title = navigationBarTitle
    }
    
    private func startFetchingCountries() {
        viewModel?.startFetchingCountries()
    }
    
    private func setupLoadingIndicatorView() {
        if #available(iOS 13.0, *) {
            self.loadingIndicatorView = UIActivityIndicatorView(style: .large)
            loadingIndicatorView?.color = .systemOrange
        }
        else {
            self.loadingIndicatorView = UIActivityIndicatorView()
            loadingIndicatorView?.color = .orange
        }
        
        loadingIndicatorView?.hidesWhenStopped = true
        
        if let loadingIndicatorView = loadingIndicatorView {
            UIViewController.addSubviewTo(view: loadingIndicatorView, superView: view, top: 0, bottom: 0, left: 0, right: 0)
        }
    }
    
    private func setupBarButtonItems() {
        let turnLeftImage = UIImage(named: Constants.ImageNames.turnLeft.rawValue)?.withRenderingMode(.alwaysOriginal)
        let leftBarItem = UIBarButtonItem(image: turnLeftImage, style: .done, target: self, action: #selector(goToPreviousPage))
        
        let turnRightImage = UIImage(named: Constants.ImageNames.turnRight.rawValue)?.withRenderingMode(.alwaysOriginal)
        let rightBarItem = UIBarButtonItem(image: turnRightImage, style: .done, target: self, action: #selector(goToNextPage))
        
        self.leftBarButtonItem = leftBarItem
        self.rightBarButtonItem = rightBarItem
    }
    
    private func configureNavigationBar() {
        if pages.count > 1 {
            navigationItem.leftBarButtonItem = leftBarButtonItem
            navigationItem.rightBarButtonItem = rightBarButtonItem
        }
        else {
            navigationItem.leftBarButtonItem = nil
            navigationItem.rightBarButtonItem = nil
        }
        
        configureNavigationBarTitle()
    }
    
    private func updatePages() {
        for country in countriesToShow.value {
            if pagesCountWithCountryName(fullname: country.fullName) == 0 {
                guard let newPageToAppend = createWeatherShowingViewControllerWithCountry(country: country) else { continue }
                
                pages.append(newPageToAppend)
            }
        }
    }
    
    private func configureMainView() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        }
        else {
            view.backgroundColor = .white
        }
    }
    
    private func showLastPage() {
        if let pageToShow = pages.last {
            firstPageWasSet = true
            pageTransitionIsInProgress = true
            
            setViewControllers([pageToShow], direction: .forward, animated: true) {[weak self] (finished) in
                if finished {
                    self?.pageTransitionIsInProgress = false
                    self?.configureNavigationBarTitle()
                }
            }
            
            currentPageIndex = pages.count - 1
        }
    }
    
    private func notifyDelegateIfNeeded() {
        if !firstFetchWasDone && (countriesToShow.value.count > 0) {
            delegateToNotify?.newCountryWasShown()
        }
    }
    
    @objc private func goToNextPage() {
        guard !pageTransitionIsInProgress else { return }
        
        pageTransitionIsInProgress = true
        var nextPage: WeatherShowingViewController!
        
        if  isLastPage() {
            nextPage = pages[0]
            currentPageIndex = 0
        }
        else {
            nextPage = pages[currentPageIndex + 1]
            currentPageIndex += 1
        }
        
        setViewControllers([nextPage], direction: .forward, animated: true)  {[weak self] (finished) in
            if finished {
                self?.pageTransitionIsInProgress = false
                self?.configureNavigationBarTitle()
            }
        }
    }
    
    @objc private func goToPreviousPage() {
        guard !pageTransitionIsInProgress else { return }
        
        pageTransitionIsInProgress = true
        var previousPage: WeatherShowingViewController!
        
        if  isFirstPage() {
            previousPage = pages[pages.count - 1]
            currentPageIndex = pages.count - 1
        }
        else {
            previousPage = pages[currentPageIndex - 1]
            currentPageIndex -= 1
        }
        
        setViewControllers([previousPage], direction: .reverse, animated: true)  {[weak self] (finished) in
            if finished {
                self?.pageTransitionIsInProgress = false
                self?.configureNavigationBarTitle()
            }
        }
    }
    
    private func createWeatherShowingViewControllerWithCountry(country: CountryToShow) -> WeatherShowingViewController? {
        guard let vcToReturn = UIViewController.viewControllerFromStoryBoard(storyboard: Constants.mainUpper, identifaerForViewController: WeatherShowingViewController.storyboardId) as? WeatherShowingViewController else { return nil }
        
        vcToReturn.setup(country: country)
        return vcToReturn
    }
    
    private func configureLoadingView() {
        isLoadingFirstCountry.value ? loadingIndicatorView?.startAnimating() : loadingIndicatorView?.stopAnimating()
    }
    
    private func isLastPage() -> Bool {
        return pages[currentPageIndex] == pages.last
    }
    
    private func isFirstPage() -> Bool {
        return pages[currentPageIndex] == pages.first
    }
    
    private func pagesCountWithCountryName(fullname: String) -> Int {
        return pages.map({ $0.countryName }).filter({ $0 == fullname }).count
    }
    
    private func configureDelegation() {
        if !firstFetchWasDone && !firstPageWasSet {
            delegate = nil
            dataSource = nil
        }
        else if firstPageWasSet && !firstFetchWasDone {
            delegate = self
            dataSource = self
            
            firstFetchWasDone = true
        }
    }
}

// MARK: UIPageViewControllerDelegate

extension WeatherListPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pageTransitionIsInProgress = true
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard finished else { return }
        
        pageTransitionIsInProgress = false
        
        guard   let currentPage = pageViewController.viewControllers?.first as? WeatherShowingViewController,
                let currentPageIndex = pages.firstIndex(of: currentPage)  else { return }
        
        self.currentPageIndex = currentPageIndex
        configureNavigationBarTitle()
    }
}

// MARK: UIPageViewControllerDataSource

extension WeatherListPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard   pages.count > 1,
                let currentPage = viewController as? WeatherShowingViewController,
                let currentPageIndex = pages.firstIndex(of: currentPage) else { return nil }
        
        if currentPage == pages.first {
            return pages.last
        }
        else {
            return pages[currentPageIndex - 1]
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard    pages.count > 1,
                let currentPage = viewController as? WeatherShowingViewController,
                let currentPageIndex = pages.firstIndex(of: currentPage) else { return nil }
        
        if currentPage == pages.last {
            return pages.first
        }
        else {
            return pages[currentPageIndex + 1]
        }
    }
}

// MARK: CountryListScreenDelegate

extension WeatherListPageViewController: CountryListScreenDelegate {
    func wasSelectedUpdateButton() {
        prepareForFirstFetching()
        startFetchingCountries()
    }
    
    func wasSelectedCountry(country: Country) {
        countryToUpdate.accept(country)
    }
    
    private func prepareForFirstFetching() {
        viewModel = nil
        initViewModel()
        setupBindings()

        pages = []
        currentPageIndex = 0
        
        firstFetchWasDone = false
        firstPageWasSet = false
        
        setViewControllers([UIViewController()], direction: .forward, animated: false, completion: nil)
        
        configureNavigationBar()
    }
}
