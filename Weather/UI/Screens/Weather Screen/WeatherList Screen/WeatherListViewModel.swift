//
//  WeatherListViewModel.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class WeatherListViewModel {
    
    // MARK: Input
    
    public let countryToUpdate: PublishRelay<Country>
    
    // MARK: Output
    
    public let loading: PublishRelay<Bool>
    public let countriesToShow: BehaviorRelay<[CountryToShow]>
    
    // MARK: Private
    
    private let locationService = LocationService()
    private let disposeBag = DisposeBag()
    
    init(countryToUpdate: PublishRelay<Country>) {
        self.countryToUpdate = PublishRelay<Country>()
           
        countryToUpdate.bind(to: self.countryToUpdate).disposed(by: disposeBag)
        
        countriesToShow = BehaviorRelay(value: [])
        loading = PublishRelay()
        
        self.countryToUpdate
            .asObservable()
            .subscribe(onNext: { [weak self] (country) in
                self?.updateDataWithCountry(country: country)
            })
            .disposed(by: disposeBag)
    }
    
    func startFetchingCountries() {
        countriesToShow.accept([])
        loading.accept(true)
        
        getAlreadyShownCountriesFromDb { [weak self] (shownCountries) in
            guard shownCountries.count != 0 else { self?.getCurrentCountryToShow();  return }
            
            var oldCountries = [CountryToShow]()
            
            if let self = self {
                oldCountries = self.countriesToShow.value
            }
            
            oldCountries.append(contentsOf: shownCountries)
            
            self?.loading.accept(false)
            self?.countriesToShow.accept(oldCountries)
        }
    }
    
    func updateDataWithCountry(country: Country) {
        var oldCountries = countriesToShow.value
        
        if !oldCountries.contains(where: { $0.shortName == country.shortName }) {
            let countryToShow = CountryToShow(country: country)

            oldCountries.append(countryToShow)
            countriesToShow.accept(oldCountries)
        }
    }
    
    // MARK: Helpers
    
    private func getCurrentCountryToShow() {
        locationService.getCurrentCountryNames { [weak self] (tuple) in
            guard let shortName = tuple?.shortName else { self?.loading.accept(false); return }
            
            CountryDataManager.shared.createShownInstanceWithShortName(shortName: shortName) { [weak self] (country) in
                if let country = country {
                    self?.loading.accept(false)
                    self?.countriesToShow.accept([country])
                }
                else {
                    guard let fullName = tuple?.fullName else { self?.loading.accept(false); return }
                    
                    CountryDataManager.shared.createShownInstanceWithFullName(fullName: fullName) { [weak self] (country) in
                        guard let country = country else { self?.loading.accept(false); return }
                        
                        self?.loading.accept(false)
                        self?.countriesToShow.accept([country])
                    }
                }
            }
        }
    }
    
    private func getAlreadyShownCountriesFromDb(completion: @escaping (_ countriesToShow: [CountryToShow]) -> () ) {
        CountryDataManager.shared.getShownInstancesFromDb { (fetchedCountries) in
            completion(fetchedCountries)
        }
    }
}
