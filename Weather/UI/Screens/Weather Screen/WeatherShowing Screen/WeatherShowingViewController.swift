//
//  WeatherShowingViewController.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class WeatherShowingViewController: UIViewController {
    
    static let storyboardId = "WeatherShowingViewController"
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableViewTopToSafeAreaConstraint: NSLayoutConstraint!
    
    private var country: CountryToShow!
    private let arrowView = UIView(frame: CGRect.zero)
    private let arrowImgView = UIImageView(frame: CGRect.zero)
    private let refreshControl = UIRefreshControl()
    private var searchBarIsShownConstraints: [NSLayoutConstraint]?
    private var arrowViewTrailingToViewTrailingConstraint: NSLayoutConstraint?
    private var viewModel: WeatherShowingViewModel?
    private let cityWeathers: BehaviorRelay<[CityWeather]?> = BehaviorRelay(value: nil)
    private let viewModelIsLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    private let error: PublishRelay<(state: WeatherDataFetchingErrorState,type: RequestError)?> = PublishRelay()
    private let shouldShowLoadingCell: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    private let disposeBag = DisposeBag()
    private var firstDataFetchWasDone = false
    private var tableViewScrollIsOnTop = true
    var countryName: String {
        self.country.fullName
    }
    private var shouldShowSearchResult = false {
        didSet {
            updateFilteredCities()
            tableView.reloadData()
        }
    }
    private var filteredCityWeathers: [CityWeather]? {
        didSet{
            if shouldShowSearchResult {
                tableView.reloadData()
            }
        }
    }

    // MARK: View Life Cycle
    
    func setup(country: CountryToShow) {
        self.country = country
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMainView()
        configureArrowView()
        configureArrowImageView()
        setArrowViewConstraints()
        setArrowImgViewConstraints()
        setupSearchBarIsShownConstraints()
        configureSearchBar()
        configureTableView()
        initViewModel()
        setupBindings()
        startFetchingWeatherData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchBar.endEditing(true)
    }
    
    // MARK: Helpers
    
    private func setupBindings() {
        viewModel?
            .cityWeathers
            .observeOn(MainScheduler.instance)
            .skip(1)
            .bind(to: cityWeathers)
            .disposed(by: disposeBag)
        
        viewModel?
            .error
            .observeOn(MainScheduler.instance)
            .bind(to: error)
            .disposed(by: disposeBag)
        
        viewModel?
            .existDataForFetching
            .skip(1)
            .observeOn(MainScheduler.instance)
            .bind(to: shouldShowLoadingCell)
            .disposed(by: disposeBag)
        
        viewModel?
            .isLoading
            .skip(1)
            .observeOn(MainScheduler.instance)
            .bind(to: viewModelIsLoading)
            .disposed(by: disposeBag)
        
        cityWeathers
            .asObservable()
            .subscribe(onNext: { [weak self] (cityWeathers) in
                self?.updateData()
                self?.setupViewsAfterFirstFetch()
            })
            .disposed(by: disposeBag)
        
        error
            .asObservable()
            .subscribe(onNext: {[weak self] (error) in
                if let error = error {
                    self?.handleError(error: error)
                }
            })
            .disposed(by: disposeBag)
        
        shouldShowLoadingCell
            .asObservable()
            .subscribe(onNext: {[weak self] (_) in
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    private func initViewModel() {
        self.viewModel = WeatherShowingViewModel(country: country)
    }
    
    private func updateData() {
        if !shouldShowSearchResult {
            tableView.reloadData()
        }
        else {
            updateFilteredCities()
        }
    }
    
    private func startFetchingWeatherData() {
        viewModel?.startFetchingWeatherData()
    }
    
    private func updateFilteredCities() {
        guard   let searchBarText = searchBar.text,
                searchBarText != "",
                let allFetchedCities = cityWeathers.value   else {
                filteredCityWeathers = cityWeathers.value ?? []
                return
        }
        
        filteredCityWeathers = allFetchedCities.filter({ $0.name.lowercased().contains(searchBarText.lowercased()) })
    }
    
    private func handleError(error: (state: WeatherDataFetchingErrorState,type: RequestError)) {
        if error.type == .weatherApiServerError {
            switch error.state {
                case .firstFetch:
                    viewModel?.startFetchingWeatherData()
                case .otherFetch:
                    viewModel?.continueFetchingWeatherData()
            }
        }
        else {
            viewModel?.endLoadingAndFetchFromDb()
        }
    }
    
    private func setupViewsAfterFirstFetch() {
        if !firstDataFetchWasDone && (cityWeathers.value != nil) {
            setupRefreshControl()
            showSearchBar()
            
            firstDataFetchWasDone = true
        }
    }
    
    private func showSearchBar() {
        guard let searchBarIsShownConstraints = searchBarIsShownConstraints else { return }
        
        NSLayoutConstraint.deactivate([tableViewTopToSafeAreaConstraint])
        NSLayoutConstraint.activate(searchBarIsShownConstraints)
        
        updateViewConstraints()
    }
    
    private func configureMainView() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        }
        else {
            view.backgroundColor = .white
        }
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        
        tableView.register(LoadingTableViewCell.self, forCellReuseIdentifier: LoadingTableViewCell.reuseId)
    }
    
    private func setupRefreshControl() {
        var attributes: [NSAttributedString.Key : Any]!
        var tintColor: UIColor!

        if #available(iOS 13.0, *) {
            attributes = [NSAttributedString.Key.foregroundColor: UIColor.systemOrange]
            tintColor = .systemOrange
        }
        else {
            attributes = [NSAttributedString.Key.foregroundColor: UIColor.orange]
            tintColor = .orange
        }
        
        refreshControl.attributedTitle = NSAttributedString(string: Constants.pullToRefresh,attributes: attributes)
        refreshControl.tintColor = tintColor
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        
        tableView.addSubview(refreshControl)
    }
    
    private func configureSearchBar() {
        searchBar.delegate = self
    }
    
    private func configureArrowImageView() {
        arrowImgView.image = UIImage(named: Constants.ImageNames.arrow.rawValue)?.withRenderingMode(.alwaysTemplate)
        arrowImgView.tintColor = UIColor.systemOrange
        arrowImgView.isUserInteractionEnabled = true

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(scrollToTheTop(_ :)))

        arrowImgView.addGestureRecognizer(tapGestureRecognizer)
        
        arrowView.addSubview(arrowImgView)
    }
    
    private func configureArrowView() {
        arrowView.layer.cornerRadius = 25
        arrowView.layer.borderWidth = 1.5
        arrowView.layer.borderColor = UIColor.orange.cgColor
        
        self.view.addSubview(arrowView)
        
        arrowView.isHidden = true
    }
    
    private func setArrowImgViewConstraints() {
        arrowImgView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            arrowImgView.topAnchor.constraint(equalTo: arrowView.topAnchor, constant: 8),
            arrowImgView.leadingAnchor.constraint(equalTo: arrowView.leadingAnchor, constant: 8),
            arrowImgView.bottomAnchor.constraint(equalTo: arrowView.bottomAnchor, constant: -8),
            arrowImgView.trailingAnchor.constraint(equalTo: arrowView.trailingAnchor, constant: -8)
        ])
    }
    
    private func setArrowViewConstraints() {
        arrowView.translatesAutoresizingMaskIntoConstraints = false
        
        let arrowViewWidthConstraint = arrowView.widthAnchor.constraint(equalToConstant: 50)
        let arrowViewHeightConstraint = arrowView.heightAnchor.constraint(equalToConstant: 50)
        let arrowViewBottomConstraint = arrowView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -35)
        let arrowViewRightConstraint = arrowView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 55)
        
        arrowViewWidthConstraint.isActive = true
        arrowViewHeightConstraint.isActive = true
        arrowViewBottomConstraint.isActive = true
        arrowViewRightConstraint.isActive = true
        
        self.arrowViewTrailingToViewTrailingConstraint = arrowViewRightConstraint
    }
    
    private func setupSearchBarIsShownConstraints() {
        guard let searchBar = searchBar else { return }
        
        let safeArea = view.safeAreaLayoutGuide
        
        let constraints = [
            NSLayoutConstraint(item: searchBar, attribute: .top, relatedBy: .equal, toItem: safeArea, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: searchBar, attribute: .leading, relatedBy: .equal, toItem: safeArea, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: searchBar, attribute: .trailing, relatedBy: .equal, toItem: safeArea, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: searchBar, attribute: .bottom, relatedBy: .equal, toItem: tableView, attribute: .top, multiplier: 1, constant: 0)
        ]
        
        self.searchBarIsShownConstraints = constraints
    }
    
    private func continueFetchingIfViewModelIsNotLoading() {
        if !viewModelIsLoading.value {
            viewModel?.continueFetchingWeatherData()
        }
    }
    
    @objc private func refresh() {
        refreshControl.endRefreshing()
        
        viewModel = nil
        initViewModel()
        setupBindings()
        
        viewModel?.startFetchingWeatherData()
    }
    
    @objc private func scrollToTheTop(_ sender: UITapGestureRecognizer) {
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
    }
}

// MARK: UITableViewDataSource

extension WeatherShowingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let cityWeathers = cityWeathers.value {
            if shouldShowSearchResult {
                guard let filteredCityWeathers = filteredCityWeathers else { return 0 }
                
                return shouldShowLoadingCell.value ? filteredCityWeathers.count + 1 : filteredCityWeathers.count
            }
            else {
                return shouldShowLoadingCell.value ? cityWeathers.count + 1 : cityWeathers.count
            }
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cityWeathers.value == nil {
            return loadingCell(tableView: tableView, indexPath: indexPath)
        }
        else {
            if shouldShowSearchResult {
                if isLoadingCellForFilteredData(indexPath: indexPath) {
                    return loadingCell(tableView: tableView, indexPath: indexPath)
                }
                else {
                    return cityWeatherCellForFilteredData(tableView: tableView, indexPath: indexPath)
                }
            }
            else {
                if isLoadingCellForNotFilteredData(indexPath: indexPath) {
                    return loadingCell(tableView: tableView, indexPath: indexPath)
                }
                else {
                    return cityWeatherCellForNotFilteredData(tableView: tableView, indexPath: indexPath)
                }
            }
        }
    }
    
    private func cityWeatherCellForNotFilteredData(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard   let cityWeatherCell = tableView.dequeueReusableCell(withIdentifier: CityWeatherTableViewCell.reuseId, for: indexPath) as? CityWeatherTableViewCell,
            let cityWeathers = cityWeathers.value else { return UITableViewCell() }
        
        let cityWeather = cityWeathers[indexPath.row]
        cityWeatherCell.setup(cityWeather: cityWeather)
        
        return cityWeatherCell
    }
    
    private func cityWeatherCellForFilteredData(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard   let cityWeatherCell = tableView.dequeueReusableCell(withIdentifier: CityWeatherTableViewCell.reuseId, for: indexPath) as? CityWeatherTableViewCell,
                let filteredCityWeathers = filteredCityWeathers   else { return UITableViewCell() }
        
        let cityWeather = filteredCityWeathers[indexPath.row]
        cityWeatherCell.setup(cityWeather: cityWeather)
        
        return cityWeatherCell
    }
    
    private func loadingCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard  let loadingCell = tableView.dequeueReusableCell(withIdentifier: LoadingTableViewCell.reuseId, for: indexPath) as? LoadingTableViewCell  else { return UITableViewCell() }
        
        loadingCell.startAnimating()
        
        return loadingCell
    }
    
    private func isLoadingCellForNotFilteredData(indexPath: IndexPath) -> Bool {
        guard   shouldShowLoadingCell.value,
                !shouldShowSearchResult,
                let cityWeathers = cityWeathers.value  else { return false }
        
        return indexPath.row == cityWeathers.count
    }
    
    private func isLoadingCellForFilteredData(indexPath: IndexPath) -> Bool {
        guard   shouldShowLoadingCell.value,
                shouldShowSearchResult,
                let filteredCityWeathers = filteredCityWeathers else { return false }
        
        return indexPath.row == filteredCityWeathers.count
    }
}

// MARK: UITableViewDelegate

extension WeatherShowingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  isLoadingCell(indexPath: indexPath) ? Constants.loadingCellHeight : Constants.cityWeatherInfoCellHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isDataLoadingCell(indexPath: indexPath)   else { return }
        
        continueFetchingIfViewModelIsNotLoading()
    }
    
    private func isLoadingCell(indexPath: IndexPath) -> Bool {
        return ((cityWeathers.value == nil) || isLoadingCellForNotFilteredData(indexPath: indexPath) || isLoadingCellForFilteredData(indexPath: indexPath))
    }
    
    private func isDataLoadingCell(indexPath: IndexPath) -> Bool {
        return (isLoadingCellForNotFilteredData(indexPath: indexPath) || isLoadingCellForFilteredData(indexPath: indexPath))
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        
        configureArrowViewVisibility(tableViewOffset: offsetY)
    }
    
    private func configureArrowViewVisibility(tableViewOffset: CGFloat) {
        if tableViewOffset > 400 {
            showArrowViewIfNeeded()
        }
        else{
            hideArrowViewIfNeeded()
        }
    }
    
    private func showArrowViewIfNeeded() {
        if tableViewScrollIsOnTop {
            arrowView.isHidden = false
            arrowViewTrailingToViewTrailingConstraint?.constant = -12
            
            UIView.animate(withDuration: 0.3) {[weak self] in
                self?.view.layoutIfNeeded()
            }
            
            tableViewScrollIsOnTop = false
        }
    }
    
    private func hideArrowViewIfNeeded() {
        if !tableViewScrollIsOnTop {
            arrowViewTrailingToViewTrailingConstraint?.constant = 55
            
            UIView.animate(withDuration: 0.3, animations: {[weak self] in
                self?.view.layoutIfNeeded()
            }) {[weak self] (isFinsihed) in
                if isFinsihed {
                    self?.arrowView.isHidden = true
                }
            }
            
            tableViewScrollIsOnTop = true
        }
    }
}

// MARK: UISearchBarDelegate

extension WeatherShowingViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResult = true
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.text?.removeAll()
        shouldShowSearchResult = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        updateFilteredCities()
    }
}
