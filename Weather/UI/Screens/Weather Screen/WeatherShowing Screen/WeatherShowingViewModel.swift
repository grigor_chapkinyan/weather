//
//  WeatherShowingViewModel.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import RxCocoa

enum WeatherDataFetchingErrorState {
    case firstFetch
    case otherFetch
}

class WeatherShowingViewModel {
    
    // MARK: Output
    
    public let cityWeathers: BehaviorRelay<[CityWeather]?>
    public let isLoading: BehaviorRelay<Bool>
    public let error: PublishRelay<(state: WeatherDataFetchingErrorState,type: RequestError)?>
    public let existDataForFetching: BehaviorRelay<Bool>
    
    // MARK: Private
    
    private let country: CountryToShow
    private var citiesToFetch: [City]?
    
    init(country: CountryToShow) {
        self.country = country
        
        cityWeathers = BehaviorRelay(value: nil)
        isLoading = BehaviorRelay(value: false)
        error = PublishRelay()
        existDataForFetching = BehaviorRelay(value: true)
    }
    
    func startFetchingWeatherData() {
        cityWeathers.accept(nil)
        existDataForFetching.accept(true)
        isLoading.accept(true)

        CityDataManager.shared.getAllForCountry(country: country, completion: { [weak self] (cities) in
            guard   let cities = cities,
                    cities.count > 0    else {
                        self?.endLoadingAndFetchFromDb()
                        return
            }
                        
            self?.citiesToFetch = cities
            
            var citiesForFirstFetching = [City]()
            var rangeToDelete = 0...0
            var isLastFetching = false
            
            if cities.count > 20 {
                citiesForFirstFetching = Array<City>(cities[0...19])
                rangeToDelete = 0...19
            }
            else {
                citiesForFirstFetching = cities
                rangeToDelete = 0...(citiesForFirstFetching.count - 1)
                isLastFetching = true
            }
            
            WeatherDataManager.shared.getDataForCities(cities: citiesForFirstFetching) { [weak self] (fetchedWeatherInfos,requestError) in
                guard   let fetchedWeatherInfos = fetchedWeatherInfos,
                        requestError == nil   else { self?.error.accept((.firstFetch,requestError ?? .other)); return }
                
                self?.configureFetchedWeatherCitysNamesWithStoredCityNames(fetchedWeather: fetchedWeatherInfos, appropriateCities: citiesForFirstFetching)
                self?.citiesToFetch?.removeSubrange(rangeToDelete)
                
                self?.cityWeathers.accept(fetchedWeatherInfos)
                self?.isLoading.accept(false)
                
                if isLastFetching {
                    self?.existDataForFetching.accept(false)
                }
            }
        })
    }
    
    func continueFetchingWeatherData() {
        isLoading.accept(true)
        
        guard   let cities = citiesToFetch,
                cities.count > 0,
                existDataForFetching.value  else {  isLoading.accept(false);  return }
        
        var rangeToDelete = 0...0
        var citiesToFetch = [City]()
        var isLastFetching = false
        
        if cities.count > 20 {
            citiesToFetch = Array<City>(cities[0...19])
            rangeToDelete = 0...19
        }
        else {
            citiesToFetch = cities
            rangeToDelete = 0...(citiesToFetch.count - 1)
            isLastFetching = true
        }
        
        WeatherDataManager.shared.getDataForCities(cities: citiesToFetch) {[weak self] (fetchedWeatherInfos, requestError) in
            guard   let fetchedWeatherInfos = fetchedWeatherInfos,
                    var citiesWeatherToUpdate = self?.cityWeathers.value,
                    requestError == nil   else { self?.error.accept((.otherFetch,requestError ?? .other)); return }
            
            self?.configureFetchedWeatherCitysNamesWithStoredCityNames(fetchedWeather: fetchedWeatherInfos, appropriateCities: citiesToFetch)
            self?.citiesToFetch?.removeSubrange(rangeToDelete)
            citiesWeatherToUpdate += fetchedWeatherInfos
            
            self?.cityWeathers.accept(citiesWeatherToUpdate)
            self?.isLoading.accept(false)
            
            if isLastFetching {
                self?.existDataForFetching.accept(false)
            }
        }
    }
    
    func endLoadingAndFetchFromDb() {
        WeatherDataManager.shared.getAllFromDbForCountry(country: country) { [weak self] (citiesWeather) in
            defer {
                self?.existDataForFetching.accept(false)
                self?.isLoading.accept(false)
            }
            
            self?.cityWeathers.accept(citiesWeather)
        }
    }
    
    // MARK: Helpers
    
    private func configureFetchedWeatherCitysNamesWithStoredCityNames(fetchedWeather: [CityWeather], appropriateCities: [City] ) {
        defer {
            DBService.sharedInstance.saveContext()
        }
        
        for cityWeather in fetchedWeather {
            guard let appropriateCity = appropriateCities.map({ $0 }).filter({ $0.id == cityWeather.id }).first else { continue }
            
            cityWeather.name = appropriateCity.name
        }
    }
}
