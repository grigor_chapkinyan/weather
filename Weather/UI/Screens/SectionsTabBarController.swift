//
//  SectionsTabBarController.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/3/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit

class SectionsTabBarController: UITabBarController {
    
    static let storyboardId = "SectionsTabBarController"

    //MARK: View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureViewControllers()
    }
    
    // MARK: Helpers
    
    private func configureViewControllers() {
        guard   let firstNav = viewControllers?.first as? UINavigationController,
                let secondNav = viewControllers?[1] as? UINavigationController,
                let countryListVC = secondNav.viewControllers.first as? CountryListViewController,
                let weatherListVC = firstNav.viewControllers.first as? WeatherListPageViewController   else { return }
        
        countryListVC.delegate = weatherListVC
        weatherListVC.delegateToNotify = countryListVC
    }
}
