//
//  CountryListViewModel.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import RxCocoa

class CountryListViewModel {
        
    // MARK: Output
    
    public let countries: BehaviorRelay<[Country]?>
    public let isLoading: BehaviorRelay<Bool>
        
    init() {
        self.countries = BehaviorRelay(value: nil)
        self.isLoading = BehaviorRelay(value: false)
    }
    
    func fetchAllCountries() {
        isLoading.accept(true)
        
        CountryDataManager.shared.getNotShownInstances { [weak self] (countries) in
            defer {
                self?.isLoading.accept(false)
            }
            
            self?.countries.accept(countries)
        }
    }
}
