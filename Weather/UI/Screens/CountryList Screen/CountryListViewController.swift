//
//  CountryListViewController.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Reachability

protocol CountryListScreenDelegate: AnyObject {
    func wasSelectedCountry(country: Country)
    func wasSelectedUpdateButton()
}

class CountryListViewController: UIViewController {
    
    static let storyboardId = "CountryListViewController"
        
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var topContainerView: UIView!
    @IBOutlet private weak var refreshButton: UIButton!
    @IBOutlet private weak var tableViewTopToSafeAreaConstraint: NSLayoutConstraint!
        
    weak var delegate: CountryListScreenDelegate?
    private var topContainerViewIsShownConstraints: [NSLayoutConstraint]?
    private var loadingIndicatorView: UIActivityIndicatorView?
    private var viewModel: CountryListViewModel?
    private let fetchedCountries: BehaviorRelay<[Country]?> = BehaviorRelay(value: nil)
    private let isLoading: BehaviorRelay<Bool> = BehaviorRelay(value: false)
    private let disposeBag = DisposeBag()
    private var firstFetchWasDone = false
    private var shouldShowSearchResult = false {
        didSet {
            updateFilteredCountries()
            tableView.reloadData()
        }
    }
    private var countriesToShow: [Country]? {
        didSet {
            updateFilteredCountries()
        }
    }
    private var filteredCountries: [Country]? {
        didSet {
            updateTableViewIfNeeded(filteredCountriesOldValue: oldValue)
        }
    }
    private var networkIsReachable: Bool {
        let reachability = try? Reachability()
        return reachability?.connection != Reachability.Connection.unavailable
    }
    
    // MARK: View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMainView()
        setupLoadingIndicatorView()
        configureRefreshButtonImage()
        configureNavigationBar()
        setupTopContainerViewIsShownConstraints()
        configureSearchBar()
        configureTabelView()
        initViewModel()
        setupBindings()
        fetchCountries()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchBar.endEditing(true)
    }
    
    // MARK: IBActions
    
    @IBAction func updateCountriesList(_ sender: Any) {
        ShowAlert.showAlertAndDismissAfter(seconds: 1, message: "Weather list was refreshed.", title: nil, viewController: self)
        
        CountryDataManager.shared.removeAllShownInstancesFromDb()
        fetchCountries()
        
        delegate?.wasSelectedUpdateButton()
    }
    
    // MARK: Helpers
    
    private func setupBindings() {
        viewModel?
            .isLoading
            .observeOn(MainScheduler.instance)
            .skip(1)
            .bind(to: isLoading)
            .disposed(by: disposeBag)
        
        viewModel?
            .countries
            .observeOn(MainScheduler.instance)
            .skip(1)
            .bind(to: fetchedCountries)
            .disposed(by: disposeBag)
        
        fetchedCountries
            .asObservable()
            .subscribe(onNext: { [weak self] (countries) in
                self?.updateData()
                self?.showSearchBarIfNeeded()
            })
            .disposed(by: disposeBag)
        
        isLoading
            .asObservable()
            .subscribe(onNext: { [weak self] (isLoading) in
                self?.configureLoadingView()
            })
            .disposed(by: disposeBag)
    }
    
    private func initViewModel() {
        viewModel = CountryListViewModel()
    }
    
    private func updateData() {
        guard let fetchedCountries = fetchedCountries.value else { return }
        
        countriesToShow = fetchedCountries
        
        if !shouldShowSearchResult {
            tableView.reloadData()
        }
    }
    
    private func fetchCountries() {
        viewModel?.fetchAllCountries()
    }
    
    private func updateFilteredCountries() {
        guard   let inputedText = searchBar.text,
                inputedText != "",
                let allCountries = countriesToShow else { filteredCountries = countriesToShow;  return }
        
        filteredCountries = allCountries.filter({ $0.fullName.lowercased().contains(inputedText.lowercased()) })
    }
    
    private func updateTableViewIfNeeded(filteredCountriesOldValue: [Country]?) {
        if (filteredCountriesOldValue != filteredCountries) && shouldShowSearchResult {
            tableView.reloadData()
        }
    }
    
    private func showSearchBarIfNeeded() {
        guard let topContainerViewIsShownConstraints = topContainerViewIsShownConstraints else { return }
        
        if (countriesToShow != nil) && !firstFetchWasDone {
            NSLayoutConstraint.deactivate([tableViewTopToSafeAreaConstraint])
            NSLayoutConstraint.activate(topContainerViewIsShownConstraints)
            
            updateViewConstraints()
            
            firstFetchWasDone = true
        }
    }
    
    private func configureMainView() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        }
        else {
            view.backgroundColor = .white
        }
    }
    
    private func setupLoadingIndicatorView() {
        if #available(iOS 13.0, *) {
            self.loadingIndicatorView = UIActivityIndicatorView(style: .large)
            loadingIndicatorView?.color = .systemOrange
        }
        else {
            self.loadingIndicatorView = UIActivityIndicatorView()
            loadingIndicatorView?.color = .orange
        }
        
        loadingIndicatorView?.hidesWhenStopped = true
        
        if let loadingIndicatorView = loadingIndicatorView {
            UIViewController.addSubviewTo(view: loadingIndicatorView, superView: view, top: 0, bottom: 0, left: 0, right: 0)
        }
    }

    private func configureNavigationBar() {
        navigationController?.navigationBar.isHidden = true
    }
    
    private func configureTabelView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        
        tableView.register(LoadingTableViewCell.self, forCellReuseIdentifier: LoadingTableViewCell.reuseId)
    }
    
    private func configureSearchBar() {
        searchBar.delegate = self
    }
    
    private func configureRefreshButtonImage() {
        if #available(iOS 13.0, *) {
            let image = UIImage(named: Constants.ImageNames.reload.rawValue)?.withTintColor(.systemOrange, renderingMode: .automatic)
            
            refreshButton.setImage(image, for: .normal)
        }
    }
    
    private func configureLoadingView() {
        isLoading.value ? loadingIndicatorView?.startAnimating() : loadingIndicatorView?.stopAnimating()
    }
    
    private func setupTopContainerViewIsShownConstraints() {
        guard let topContainerView = topContainerView else { return }
        
        let safeArea = view.safeAreaLayoutGuide
        
        let constraints = [
            NSLayoutConstraint(item: topContainerView, attribute: .top, relatedBy: .equal, toItem: safeArea, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: topContainerView, attribute: .leading, relatedBy: .equal, toItem: safeArea, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: topContainerView, attribute: .trailing, relatedBy: .equal, toItem: safeArea, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: topContainerView, attribute: .bottom, relatedBy: .equal, toItem: tableView, attribute: .top, multiplier: 1, constant: 0)
        ]
        
        self.topContainerViewIsShownConstraints = constraints
    }
}

// MARK: UITableViewDataSource

extension CountryListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let countries = countriesToShow {
            if shouldShowSearchResult {
                guard let filteredCountries = filteredCountries else { return 0 }
                
                return filteredCountries.count
            }
            else {
                return countries.count
            }
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if countriesToShow == nil {
            return loadingCell(tableView: tableView, indexPath: indexPath)
        }
        else {
            if shouldShowSearchResult {
                return countryCellForFilteredData(tableView: tableView, indexPath: indexPath)
            }
            else {
                return countryCellForNotFilteredData(tableView: tableView, indexPath: indexPath)
            }
        }
    }
    
    private func countryCellForNotFilteredData(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let countryCell = tableView.dequeueReusableCell(withIdentifier: CountryInfoTableViewCell.reuseId, for: indexPath) as? CountryInfoTableViewCell,
            let countryToSend = countriesToShow?[indexPath.row] else { return UITableViewCell() }
                
        countryCell.setup(country: countryToSend)
        return countryCell
    }
    
    private func countryCellForFilteredData(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard   let countryCell = tableView.dequeueReusableCell(withIdentifier:CountryInfoTableViewCell.reuseId, for: indexPath) as? CountryInfoTableViewCell,
                let countryToSend = filteredCountries?[indexPath.row] else { return UITableViewCell() }
        
        countryCell.setup(country: countryToSend)
        return countryCell
    }
    
    private func loadingCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard  let loadingCell = tableView.dequeueReusableCell(withIdentifier: LoadingTableViewCell.reuseId, for: indexPath) as? LoadingTableViewCell  else { return UITableViewCell() }
        
        loadingCell.startAnimating()
        return loadingCell
    }
}

// MARK: UITableViewDelegate

extension CountryListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (countriesToShow == nil) ? Constants.loadingCellHeight : Constants.countryInfoCellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard networkIsReachable else {
            ShowAlert.showAlert(message: Constants.AlertMessage.checkInternet.rawValue, title: Constants.AlertTitle.noInternet.rawValue, viewController: self)
            
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        removeItemAtIndexPath(indexPath: indexPath)
    }
    
    private func removeItemAtIndexPath(indexPath: IndexPath) {
        guard   let countryToRemove = shouldShowSearchResult ? filteredCountries?[indexPath.row] : countriesToShow?[indexPath.row],
                let indexInNotFilteredData = countriesToShow?.firstIndex(of: countryToRemove)  else { return }
        
        delegate?.wasSelectedCountry(country: countryToRemove)
        ShowAlert.showAlertAndDismissAfter(seconds: 1, message: "\(countryToRemove.fullName) is added to weather list.", title: nil, viewController: self)
                
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .left)
        countriesToShow?.remove(at: indexInNotFilteredData)
        tableView.endUpdates()
    }
}

// MARK: UISearchBarDelegate

extension CountryListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        shouldShowSearchResult = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.text?.removeAll()
        shouldShowSearchResult = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        updateFilteredCountries()
    }
}

// MARK: WeatherShowingScreenDelegate

extension CountryListViewController: WeatherShowingScreenDelegate {
    func newCountryWasShown() {
        fetchCountries()
    }
}
