//
//  CountryToShow.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/15/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(CountryToShow)
class CountryToShow: NSManagedObject {
    
    @NSManaged var fullName: String
    @NSManaged var shortName: String

    required convenience init(country: Country)  {
        defer {
            DBService.sharedInstance.saveContext()
        }
        
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.countryToShow.rawValue, in: DBService.sharedInstance.managedContext)  else { fatalError(Constants.ErrorMessage.decodeError.rawValue + " in CountryToShow model")}
        
        DBService.sharedInstance.deleteEntityIfExist(entityName: Constants.CoreDataEntity.countryToShow.rawValue, propertyTypeForPredicate: .fullName, propertyValue: country.fullName)
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)
        
        self.fullName = country.fullName
        self.shortName = country.shortName
    }
}
