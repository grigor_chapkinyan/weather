//
//  WeatherDescription.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(WeatherDescription)
class WeatherDescription: NSManagedObject {
    
    @NSManaged var icon : String
    
    required convenience init?(dictionary: NSDictionary) {
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.weatherDescription.rawValue, in: DBService.sharedInstance.managedContext),
                let icon = dictionary[Constants.Key.icon.rawValue] as? String   else { return nil }
        
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)
        
        self.icon = icon
    }
}
