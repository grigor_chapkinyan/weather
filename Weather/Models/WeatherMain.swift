//
//  WeatherMain.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(WeatherMain)
class WeatherMain: NSManagedObject {
    
    @NSManaged var temp : Double
    
    static func modelsFromDictionaryArray(array:NSArray) -> [WeatherMain] {
        var models:[WeatherMain] = []
        
        for item in array {
            guard   let item = item as? NSDictionary,
                    let main = WeatherMain(dictionary: item) else { continue }
            
            models.append(main)
        }
        return models
    }
    
    required convenience init?(dictionary: NSDictionary) {
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.weatherMain.rawValue, in: DBService.sharedInstance.managedContext),
                let temp = dictionary[Constants.Key.temp.rawValue] as? Double else { return nil }
        
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)

        self.temp = temp
    }
}
