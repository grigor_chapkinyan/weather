//
//  CityWeatherList.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/3/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

struct CityWeatherList {
    
    let cnt : Int
    let list : Array<CityWeather>

    init?(dictionary: NSDictionary) {
        guard   let cnt = dictionary[Constants.Key.cnt.rawValue] as? Int,
                let listArray = dictionary[Constants.Key.list.rawValue] as? NSArray  else { return nil }
        
        list = CityWeather.modelsFromDictionaryArray(array: listArray)
        self.cnt = cnt
    }
}
