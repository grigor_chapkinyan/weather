//
//  Country.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/31/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(Country)
class Country: NSManagedObject, Decodable {
    
    enum CodingKeys: String, CodingKey {
        case fullName = "Name"
        case shortName = "Code"
    }
    
    static func ==(lhs: Country, rhs: Country) -> Bool {
        return lhs.shortName == rhs.shortName
    }
    
    @NSManaged var fullName: String
    @NSManaged var shortName: String
    
    // MARK: Decodable
    
    required convenience init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let fullName = try container.decode(String.self, forKey: .fullName)
        
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.country.rawValue, in: DBService.sharedInstance.managedContext)  else { fatalError(Constants.ErrorMessage.decodeError.rawValue + " in Country model")}
        
        DBService.sharedInstance.deleteEntityIfExist(entityName: Constants.CoreDataEntity.country.rawValue, propertyTypeForPredicate: .fullName, propertyValue: fullName)
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)
        
        self.fullName = fullName
        self.shortName = try container.decode(String.self, forKey: .shortName)
    }
}
