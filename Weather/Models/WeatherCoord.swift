//
//  WeatherCoord.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(WeatherCoord)
class WeatherCoord: NSManagedObject {
    
    @NSManaged var lon : Double
    @NSManaged var lat : Double
    
    static func modelsFromDictionaryArray(array:NSArray) -> [WeatherCoord] {
        var models:[WeatherCoord] = []
        
        for item in array
        {
            models.append(WeatherCoord(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required convenience init?(dictionary: NSDictionary) {
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.weatherCoord.rawValue, in: DBService.sharedInstance.managedContext),
                let lon = dictionary[Constants.Key.lon.rawValue] as? Double,
                let lat = dictionary[Constants.Key.lat.rawValue] as? Double else { return nil }
        
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)

        self.lon = lon
        self.lat = lat
    }
}
