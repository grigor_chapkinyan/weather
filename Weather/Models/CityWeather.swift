//
//  CityWeather.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(CityWeather)
class CityWeather: NSManagedObject {
    
    @NSManaged var coord : WeatherCoord
    @NSManaged var sys : WeatherSys
    @NSManaged var weather : WeatherDescription
    @NSManaged var main : WeatherMain
    @NSManaged var id : Int32
    @NSManaged var name : String
    
    static func ==(lhs: CityWeather, rhs: CityWeather) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func modelsFromDictionaryArray(array:NSArray) -> [CityWeather] {
        var models:[CityWeather] = []
        for item in array {
            guard let item = item as? NSDictionary,
                  let cityWeather = CityWeather(dictionary: item)  else { continue }
            
            models.append(cityWeather)
        }
        return models
    }
    
    required convenience init?(dictionary: NSDictionary) {
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.cityWeather.rawValue, in: DBService.sharedInstance.managedContext),
            let coord = dictionary[Constants.Key.coord.rawValue] as? NSDictionary,
                let sys = dictionary[Constants.Key.sys.rawValue] as? NSDictionary,
                let weatherDescriptionDict = (dictionary[Constants.Key.weather.rawValue] as? NSArray)?.firstObject as? NSDictionary,
                let main = dictionary[Constants.Key.main.rawValue] as? NSDictionary,
                let id = dictionary[Constants.Key.id.rawValue] as? Int32,
                let name = dictionary[Constants.Key.name.rawValue] as? String,
                let weatherDescObject = WeatherDescription(dictionary: weatherDescriptionDict),
                let coordObject = WeatherCoord(dictionary: coord),
                let sysObject = WeatherSys(dictionary: sys),
                let mainObject = WeatherMain(dictionary: main)   else { return nil }
        
        DBService.sharedInstance.deleteEntityIfExist(entityName: Constants.CoreDataEntity.cityWeather.rawValue, propertyTypeForPredicate: .id, propertyValue: String(id))
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)
        
        self.coord = coordObject
        self.sys = sysObject
        self.weather = weatherDescObject
        self.main = mainObject
        self.id = id
        self.name = name
    }
}
