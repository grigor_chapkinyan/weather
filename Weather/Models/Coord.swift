//
//  Coord.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/4/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

struct Coord: Decodable {
    
    enum CodingKeys: CodingKey {
        case lon
        case lat
    }
    
    let lon: Double
    let lat: Double
}
