//
//  City.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

struct City: Decodable, Equatable {
    
    enum CodingKeys: CodingKey {
        case name
        case id
        case state
        case country
        case coord
    }
    
    static func ==(lhs: City, rhs: City) -> Bool {
        return lhs.name == rhs.name
    }
    
    let name: String
    let id: Int32
    let state: String
    let country: String
    let coord: Coord
}




