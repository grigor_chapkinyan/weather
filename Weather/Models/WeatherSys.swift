//
//  WeatherSys.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import CoreData

@objc(WeatherSys)
class WeatherSys: NSManagedObject {
    
    @NSManaged var country : String
    
    required convenience init?(dictionary: NSDictionary) {
        guard   let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataEntity.weatherSys.rawValue, in: DBService.sharedInstance.managedContext),
                let country = dictionary[Constants.Key.country.rawValue] as? String   else { return nil }
        
        self.init(entity: entity, insertInto: DBService.sharedInstance.managedContext)

        self.country = country
    }
}
