//
//  CountryDataManager.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/3/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

class CountryDataManager {
    
    static let shared = CountryDataManager()
    private init() {}
    
    private var helperQueue = DispatchQueue.global(qos: .default)
    
    func createShownInstanceWithFullName(fullName: String, completion: @escaping (_ countryToShow: CountryToShow?) -> () ) {
        CountryDataManager.shared.getAllSortedByFullname { (countries) in
            guard   let countryToReturn = countries.filter({ $0.fullName == fullName }).first,
                    (!countryToReturn.shortName.isEmpty && !countryToReturn.fullName.isEmpty)   else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            let countryToShow = CountryToShow(country: countryToReturn)
            DispatchQueue.main.async {
                completion(countryToShow)
            }
        }
    }
    
    func createShownInstanceWithShortName(shortName: String, completion: @escaping (_ countryToShow: CountryToShow?) -> () ) {
        CountryDataManager.shared.getAllSortedByFullname { (countries) in
            guard   let countryToReturn = countries.filter({ $0.shortName == shortName }).first,
                (!countryToReturn.shortName.isEmpty && !countryToReturn.fullName.isEmpty)   else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            let countryToShow = CountryToShow(country: countryToReturn)
            DispatchQueue.main.async {
                completion(countryToShow)
            }
        }
    }
    
    func getShownInstancesFromDb(completion: @escaping (_ countries: [CountryToShow]) -> () ) {
        DBService.sharedInstance.getEntitiesAsync(name: Constants.CoreDataEntity.countryToShow.rawValue, predicate: nil, sortDecriptors: nil) { (fetchedObjects) in
            guard let countriesToShow = fetchedObjects as? [CountryToShow] else {
                print(Constants.ErrorMessage.fetchError.rawValue)
                
                DispatchQueue.main.async {
                    completion([])
                }
                return
            }
            
            DispatchQueue.main.async {
                completion(countriesToShow.reversed())
            }
        }
    }
    
    func getNotShownInstances(completion: @escaping (_ countries: [Country]) -> () ) {
        CountryDataManager.shared.getAllSortedByFullname { (allCountries) in
            CountryDataManager.shared.getShownInstancesFromDb { [weak self] (allShownCountries) in
                self?.helperQueue.async { [weak self] in
                    guard let self = self else {
                        DispatchQueue.main.async {
                            completion([])
                        }
                        return
                    }
                    
                    let arrayToReturn = self.notShownInstancesFromArray(allCountries: allCountries, shownCountries: allShownCountries)
                    
                    DispatchQueue.main.async {
                        completion(arrayToReturn)
                    }
                }
            }
        }
    }
    
    func removeAllShownInstancesFromDb() {
        DBService.sharedInstance.deleteEntityIfExist(entityName: Constants.CoreDataEntity.countryToShow.rawValue, propertyTypeForPredicate: nil, propertyValue: nil)
        
        WeatherDataManager.shared.removeAllFromDb()
    }
    
    func getAllSortedByFullname(completion: @escaping (_ countries: [Country]) -> () ) {
        fetchAllFromDb { [weak self] (countries) in
            guard let self =  self else { completion([]); return }
            
            if countries.count > 0 {
                completion(countries)
            }
            else {
                let fetchedCountries = self.fetchAllFromJson()
                
                completion(fetchedCountries)
            }
        }
    }
    
    // MARK: Helpers
    
    private func countriesMapping(data: Data) -> [Country]? {
        defer {
            DBService.sharedInstance.saveContext()
        }
        
        let decoder = JSONDecoder()
        return try? decoder.decode([Country].self, from: data)
    }
    
    private func configureFetchedCountriesToReturn(countries: [Country]) -> [Country] {
        let arrayToReturn = countries.removeDuplicatesIfExist()
        
        return arrayToReturn.sorted(by: { $0.fullName < $1.fullName })
    }
    
    private func fetchAllFromDb(completion: @escaping (_ countries: [Country]) -> () ) {
        let sortDescriptor = NSSortDescriptor(key: Constants.Key.fullName.rawValue, ascending: true)
        
        DBService.sharedInstance.getEntitiesAsync(name: Constants.CoreDataEntity.country.rawValue, predicate: nil, sortDecriptors: [sortDescriptor]) { (fetchedObjects) in
            guard let countries = fetchedObjects as? [Country] else {
                print(Constants.ErrorMessage.fetchError.rawValue)
                completion([])
                return
            }
            
            completion(countries)
        }
    }
    
    private func notShownInstancesFromArray(allCountries: [Country], shownCountries: [CountryToShow]) -> [Country] {
        let result = allCountries.filter({ (countryIter) -> Bool in
            for shownCountryIter in shownCountries {
                if countryIter.fullName == shownCountryIter.fullName {
                    return false
                }
            }
            
            return true
        })
        
        return result
    }
    
    private func fetchAllFromJson() -> [Country] {
            guard   let countriesUrl = Bundle.main.url(forResource: Constants.ResourceName.countries.rawValue, withExtension: Constants.FileFormat.json.rawValue),
                    let countriesData = try? Data(contentsOf: countriesUrl)  else { return [] }
            
            var countriesToReturn = [Country]()
            
            if let allCountries = countriesMapping(data: countriesData) {
                let countries = configureFetchedCountriesToReturn(countries: allCountries)
                
                countriesToReturn = countries
            }
            
            return countriesToReturn
    }
}
