//
//  CityDataManager.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/14/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

class CityDataManager {
    
    static let shared = CityDataManager()
    private init() {}
    
    private let helperQueue = DispatchQueue.global(qos: .default)
    
    func getAllForCountry(country: CountryToShow, completion: @escaping (_ cities: [City]?) -> () ) {
        helperQueue.async { [weak self] in
            guard   let citiesUrl = Bundle.main.url(forResource: Constants.ResourceName.cities.rawValue, withExtension: Constants.FileFormat.json.rawValue),
                    let citiesData = try? Data(contentsOf: citiesUrl, options: .uncached)  else {
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                    return
            }
            
            var citiesToReturn: [City]?

            if let allCities = self?.citiesMapping(data: citiesData) {
                let citiesForCurrentCountry = allCities.filter({ $0.country == country.shortName })
                
                citiesToReturn = self?.configureFetchedCitiesToReturn(cities: citiesForCurrentCountry)
            }
            
            DispatchQueue.main.async {
                completion(citiesToReturn)
            }
        }
    }
    
    // MARK: Helpers
    
    private func citiesMapping(data: Data) -> [City]? {
        let decoder = JSONDecoder()
        return try? decoder.decode([City].self, from: data)
    }
    
    private func configureFetchedCitiesToReturn(cities: [City]) -> [City] {
        let arrayToReturn = cities.removeDuplicatesIfExist()
        
        return arrayToReturn.sorted(by: { $0.name < $1.name })
    }
}
