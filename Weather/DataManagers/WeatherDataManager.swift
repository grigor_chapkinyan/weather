//
//  WeatherDataManager.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

class WeatherDataManager {
    
    static var shared = WeatherDataManager()
    private init() {}
    
    private let locationService = LocationService()
    private let networkService = NetworkService()
    
    func getDataForCities(cities: [City], completion: @escaping (_ weathers: [CityWeather]?,_ error: RequestError?) -> () ) {
        guard   cities.count <= 20,
                cities.count > 0 else {
                    completion(nil,.other)
                    return
        }
        
        let urlStringForFetching = urlStringForFetchingCities(cities: cities)
        
        networkService.requestData(urlString: urlStringForFetching, method: .get) { [weak self] (data,error) in
            guard   let data = data,
                    let citiesWeather = self?.citiesWeatherFromDataContainer(data: data)  else {
                        completion(nil,error)
                        return
            }
            
            completion(citiesWeather,error)
        }
    }
    
    func getAllFromDbForCountry(country: CountryToShow, completion: @escaping (_ dataToReturn: [CityWeather]) -> () ) {
        let sortDescriptor = NSSortDescriptor(key: Constants.Key.name.rawValue, ascending: true)
        let predicate = NSPredicate(format: "sys.country == %@", country.shortName)
        
        DBService.sharedInstance.getEntitiesAsync(name: Constants.CoreDataEntity.cityWeather.rawValue, predicate: predicate, sortDecriptors: [sortDescriptor]) { (fetchedObjects) in
            guard let citiesWeather = fetchedObjects as? [CityWeather] else {
                completion([]);
                print(Constants.ErrorMessage.fetchError.rawValue)
                return
            }
            
            completion(citiesWeather)
        }
    }
    
    func removeAllFromDb() {
        DBService.sharedInstance.deleteEntityIfExist(entityName: Constants.CoreDataEntity.cityWeather.rawValue, propertyTypeForPredicate: nil, propertyValue: nil)
    }
    
    // MARK: Helpers
    
    private func citiesWeatherFromDataContainer(data: Data) -> [CityWeather]? {
        defer {
            DBService.sharedInstance.saveContext()
        }
        
        let jsonResult = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
        
        if  let jsonResult = jsonResult as? NSDictionary,
            let cityWeatherList = CityWeatherList(dictionary: jsonResult) {
            
            return cityWeatherList.list
        }
        
        return nil
    }
    
    private func urlStringForFetchingCities(cities: [City]) -> String {
        var cityIdsUrlPathToAppend = ""
        
        for (index, city) in cities.enumerated() {
            if index == 0 {
                let cityIdString = String(city.id)
                cityIdsUrlPathToAppend.append(cityIdString)
            }
            else {
                let cityIdString = String(city.id)
                
                cityIdsUrlPathToAppend.append("," + cityIdString)
            }
        }
        
        var baseUrlPath = Constants.WeatherApi.baseUrlPathForSeveralCityFetchById.rawValue
        let apiKeyUrlPathToAppend = Constants.WeatherApi.keyUrlPathToAppend.rawValue
        let unitsUrlPathToAppend = Constants.WeatherApi.unitsUrlPathToAppend.rawValue
        
        baseUrlPath += cityIdsUrlPathToAppend
        baseUrlPath += unitsUrlPathToAppend
        baseUrlPath += apiKeyUrlPathToAppend
        
        return baseUrlPath
    }
}
