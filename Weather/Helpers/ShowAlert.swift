//
//  ShowAlert.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit

class ShowAlert {
    
    class func showAlert(message: String?, title: String?, viewController: UIViewController? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Constants.LabelNames.ok.rawValue , style: .default)
        alertController.addAction(action)
        
        if let viewController = viewController {
            viewController.present(alertController, animated: true, completion: nil)
        }
        else {
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        }
    }
    
    class func showAlertAndDismissAfter(seconds: Int, message: String?, title: String?, viewController: UIViewController? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let viewController = viewController {
            viewController.present(alertController, animated: true, completion: nil)
        }
        else {
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        }
        
        let deadlineTime = DispatchTime.now() + .seconds(seconds)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            alertController.dismiss(animated: true, completion: nil)
        }
    }
}
