//
//  NetworkService + Ext.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/1/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

extension NetworkService {
    enum HTTPMethod: String {
        case options = "OPTIONS"
        case get     = "GET"
        case head    = "HEAD"
        case post    = "POST"
        case put     = "PUT"
        case patch   = "PATCH"
        case delete  = "DELETE"
        case trace   = "TRACE"
        case connect = "CONNECT"
    }
}
