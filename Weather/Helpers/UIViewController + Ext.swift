//
//  UIViewController + Ext.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    static func addSubviewTo(view: UIView, superView: UIView, top: Int, bottom: Int, left: Int, right: Int) {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        superView.addSubview(view)
        
        let constraints = [
            NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: superView, attribute: .bottom, multiplier: 1.0, constant: CGFloat(bottom)),
            NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: superView, attribute: .top, multiplier: 1.0, constant: CGFloat(top)),
            NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: superView, attribute: .leading, multiplier: 1.0, constant: CGFloat(left)),
            NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: superView, attribute: .trailing, multiplier: 1.0, constant: CGFloat(right))
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    static func viewControllerFromStoryBoard(storyboard: String, identifaerForViewController: String) -> UIViewController {
        return UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifaerForViewController)
    }
}


