//
//  Array + Ext.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/5/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    func removeDuplicatesIfExist() -> [Element] {
        var arrayToReturn = [Element]()
        
        for element in self {
            if !arrayToReturn.contains(element) {
                arrayToReturn.append(element)
            }
        }
        
        return arrayToReturn
    }
}
