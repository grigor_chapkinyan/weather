//
//  ReachabilityObserverDelegate.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 4/2/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import Foundation
import Reachability

fileprivate var reachability: Reachability?

protocol ReachabilityActionDelegate {
    func reachabilityChanged(_ isReachable: Bool)
}

protocol ReachabilityObserverDelegate: class, ReachabilityActionDelegate {
    func addReachabilityObserver() throws
    func removeReachabilityObserver()
}

extension ReachabilityObserverDelegate {
    
    func addReachabilityObserver() throws {
        reachability = try Reachability()
        
        reachability?.whenReachable = { [weak self] reachability in
            self?.reachabilityChanged(true)
        }
        
        reachability?.whenUnreachable = { [weak self] reachability in
            self?.reachabilityChanged(false)
        }
        
        try reachability?.startNotifier()
    }
    
    func removeReachabilityObserver() {
        reachability?.stopNotifier()
        reachability = nil
    }
}
