//
//  AppDelegate.swift
//  Weather
//
//  Created by Grigor Chapkinyan on 3/30/20.
//  Copyright © 2020 Grigor Chapkinyan. All rights reserved.
//

import UIKit
import Reachability

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, ReachabilityObserverDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        try? addReachabilityObserver()
        
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        removeReachabilityObserver()
        DBService.sharedInstance.saveContext()
    }
}

// MARK: ReachabilityObserverDelegate

extension AppDelegate {
    
    func reachabilityChanged(_ isReachable: Bool) {
        if !isReachable {
            ShowAlert.showAlert(message: Constants.AlertMessage.checkInternet.rawValue, title: Constants.AlertTitle.noInternet.rawValue)
        }
    }
}

